/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 * Author: Rohan Garg <rohan.garg@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include "gpu_ds.h"

#include <cerrno>
#include <cstring>
#include <thread>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <drm/panfrost_drm.h>
#include <xf86drm.h>

#include "mali_counter_names.h"

#define MAX_DRM_DEVICES 64

namespace pps::gpu
{

uint64_t PanfrostDataSource::sampling_period_ns = PanfrostDataSource::min_sampling_period_ns;

static State state = State::Stop;

State PanfrostDataSource::get_state()
{
	return state;
}

/// @brief Checks whether a return value is valid
/// @param res Result from a syscall
/// @param msg Message to prepend to strerror
/// @return True if ok, false otherwise
bool check( int res, const char* msg )
{
	if ( res < 0 )
	{
		char* err_msg = std::strerror( errno );
		PERFETTO_ELOG( "%s: %s", msg, err_msg );
		return false;
	}

	return true;
}

std::vector<const char*> get_counter_names( int card_fd )
{
	if (card_fd <= 0)
	{
		PERFETTO_FATAL("Invalid GPU file descriptor");
		return std::vector<const char*>();
	}

	struct drm_panfrost_get_param get_param = {0,};
	get_param.param = DRM_PANFROST_PARAM_GPU_PROD_ID;
	auto ret = drmIoctl(card_fd, DRM_IOCTL_PANFROST_GET_PARAM, &get_param);

	if ( !check(ret, "Could not query GPU ID") )
	{
		return std::vector<const char*>();
	}

	int size = 0;

	switch (get_param.value)
	{
	case 0x600:
		size = sizeof( hardware_counters_mali_t60x ) / sizeof ( hardware_counters_mali_t60x[0] );
		return std::vector<const char*>(hardware_counters_mali_t60x,
										hardware_counters_mali_t60x + size);
	case 0x620:
		size = sizeof( hardware_counters_mali_t62x ) / sizeof ( hardware_counters_mali_t62x[0] );
		return std::vector<const char*>(hardware_counters_mali_t62x,
										hardware_counters_mali_t62x + size);
	case 0x720:
		size = sizeof( hardware_counters_mali_t72x ) / sizeof ( hardware_counters_mali_t72x[0] );
		return std::vector<const char*>(hardware_counters_mali_t72x,
										hardware_counters_mali_t72x + size);
	case 0x750:
		size = sizeof( hardware_counters_mali_t76x ) / sizeof ( hardware_counters_mali_t76x[0] );
		return std::vector<const char*>(hardware_counters_mali_t76x,
										hardware_counters_mali_t76x + size);
	case 0x820:
		size = sizeof( hardware_counters_mali_t82x ) / sizeof ( hardware_counters_mali_t82x[0] );
		return std::vector<const char*>(hardware_counters_mali_t82x,
										hardware_counters_mali_t82x + size);
	case 0x830:
		size = sizeof( hardware_counters_mali_t83x ) / sizeof ( hardware_counters_mali_t83x[0] );
		return std::vector<const char*>(hardware_counters_mali_t83x,
										hardware_counters_mali_t83x + size);
	case 0x860:
		size = sizeof( hardware_counters_mali_t86x ) / sizeof ( hardware_counters_mali_t86x[0] );
		return std::vector<const char*>(hardware_counters_mali_t86x,
										hardware_counters_mali_t86x + size);
	case 0x880:
		size = sizeof( hardware_counters_mali_t88x ) / sizeof ( hardware_counters_mali_t88x[0] );
		return std::vector<const char*>(hardware_counters_mali_t88x,
										hardware_counters_mali_t88x + size);
	}

	return std::vector<const char*>();
}

/// @brief Allocates memory to store data from GPU
void PanfrostDataSource::OnSetup( const SetupArgs& args )
{
	const std::string& config_raw = args.config->gpu_counter_config_raw();
	perfetto::protos::pbzero::GpuCounterConfig::Decoder config(config_raw);

	if (config.has_counter_period_ns())
	{
		sampling_period_ns = config.counter_period_ns();
		if ( sampling_period_ns < min_sampling_period_ns )
		{
			PERFETTO_ELOG( "Sampling period should be greater than 1000000 ns (1 ms)" );
			PERFETTO_ELOG( "Sampling period set to 1 ms to minimize impact intrusiveness of tracing" );
			sampling_period_ns = min_sampling_period_ns;
		}
	}

	/// @todo Enable only counters specified by config.counter_ids()

	PERFETTO_LOG( "Initialization finished" );
}

bool fdIsPanfrost(const int fd)
{
	drmVersionPtr version = drmGetVersion(fd);
	bool ret = false;

	if ( version && strcmp(version->name, "panfrost") == 0 )
	{
		ret = true;
		drmFreeVersion(version);
	}

	return ret;
}

int OpenCard()
{
	drmDevicePtr devices[MAX_DRM_DEVICES], device;
	drmVersionPtr version;
	int i, num_devices, fd = -1;

	num_devices = drmGetDevices2(0, devices, MAX_DRM_DEVICES);
	if (num_devices <= 0)
	{
		PERFETTO_LOG("Cound not get any DRM devices.");
		return fd;
	}

	for ( i = 0; i < num_devices; i++ )
	{
		device = devices[i];
		if ( (device->available_nodes & (1 << DRM_NODE_RENDER) ) &&
			(device->bustype == DRM_BUS_PLATFORM))
		{
			fd = open( device->nodes[DRM_NODE_RENDER], O_RDONLY );
			if ( fd < 0 )
				continue;

			if ( fdIsPanfrost(fd) )
				break;

			close(fd);
			fd = -1;
		}
	}

	drmFreeDevices(devices, num_devices);
	return fd;
}

/// @brief Enables performance counters
void PanfrostDataSource::OnStart( const StartArgs& args )
{
	if ( card_fd < 0 )
	{
		card_fd = OpenCard();
		if ( !check( card_fd, "Cannot open graphics card" ) )
		{
			PERFETTO_FATAL( "Please verify path" );
		}
	}

	if ( !counters )
	{
		counter_names = get_counter_names(card_fd);

		if ( counter_names.empty() )
		{
			PERFETTO_FATAL( "Could not query for counters" );
		}

		counters = allocator.allocate( 2 *  counter_names.size());
	}

	drm_panfrost_perfcnt_enable perfcnt = {};
	perfcnt.enable = 1;
	perfcnt.counterset = 0;

	auto res = drmIoctl( card_fd, DRM_IOCTL_PANFROST_PERFCNT_ENABLE, &perfcnt );
	if ( !check( res, "Cannot enable perfcnt" ) )
	{
		if ( res == -ENOSYS )
		{
			PERFETTO_FATAL( "Please enable unstable ioctls with: modprobe panfrost unstable_ioctls=1" );
		}
		PERFETTO_FATAL( "Please verify graphics card" );
	}

	state = State::Start;

	PERFETTO_LOG( "OnStart finished" );
}

void close_callback( PanfrostDataSource::TraceContext ctx )
{
	auto packet = ctx.NewTracePacket();
	packet->Finalize();
	ctx.Flush();
	PERFETTO_LOG( "Context flushed" );
}

/// @brief Disables performance counters
void PanfrostDataSource::OnStop( const StopArgs& args )
{
	Trace( close_callback );

	drm_panfrost_perfcnt_enable perfcnt = {};
	perfcnt.enable = 0; // disable

	auto res = drmIoctl( card_fd, DRM_IOCTL_PANFROST_PERFCNT_ENABLE, &perfcnt );
	check( res, "Cannot disable perfcnt" );

	close( card_fd );
	card_fd = -1;

	state = State::Stop;

	PERFETTO_LOG( "OnStop finished" );
}


/// @return The last sampled value for the counter
counter_t PanfrostDataSource::get_counter( const Counter counter )
{
	auto name_found = std::find_if(
		counter_names.begin(),
		counter_names.end(),
		[counter] ( const char* name ) {
			return std::strcmp( name + 5, to_string( counter ) ) == 0;
		}
	);

	if ( name_found == counter_names.end() )
	{
		PERFETTO_ELOG( "Cannot find %s", to_string( counter ) );
		return 0;
	}

	auto offset = name_found - counter_names.begin();
	return counters[offset];
}

void PanfrostDataSource::dump()
{
	// Dump performance counters to buffer
	drm_panfrost_perfcnt_dump dump = {};
	dump.buf_ptr = reinterpret_cast<uintptr_t>( counters );

	auto res = drmIoctl( card_fd, DRM_IOCTL_PANFROST_PERFCNT_DUMP, &dump );
	if ( !check( res, "Cannot dump" ) )
	{
		PERFETTO_ELOG( "Skipping sample" );
		return;
	}
}


/// @brief Sends GPU counter blocks (or categories)
void add_blocks( perfetto::protos::pbzero::GpuCounterDescriptor& desc )
{
	for ( uint32_t block_index = 0;
	      block_index < static_cast<uint32_t>( CounterBlock::MAX_VALUE );
	      ++block_index )
	{
		auto block = static_cast<CounterBlock>( block_index );

		// Define block properties
		auto block_desc = desc.add_blocks();
		block_desc->set_name( to_string( block ) );
		block_desc->set_block_id( block_index );

		for ( uint32_t counter_index = 0;
		      counter_index < static_cast<uint32_t>( Counter::MAX_VALUE );
		      ++counter_index )
		{
			// Associate counters to blocks
			auto counter = static_cast<Counter>( counter_index );
			if ( get_block( counter ) == block )
			{
				block_desc->add_counter_ids( counter_index );
			}
		}
	}
}


void PanfrostDataSource::trace_callback( TraceContext ctx )
{
	using namespace perfetto::protos::pbzero;

	auto data_source = ctx.GetDataSourceLocked();
	if ( !data_source )
	{
		// Data source has been destroyed, just finish
		return;
	}

	// Dump performance counters
	data_source->dump();

	auto state = ctx.GetIncrementalState();

	auto packet = ctx.NewTracePacket();
	packet->set_timestamp( perfetto::base::GetBootTimeNs().count() );

	auto event = packet->set_gpu_counter_event();
	event->set_gpu_id( 0 );

	GpuCounterDescriptor* desc = nullptr;

	if ( state->was_cleared )
	{
		/// @todo Do we need to send descriptions again
		/// when restarting this producer during a perfetto tracing session?
		desc = event->set_counter_descriptor();
		desc->set_min_sampling_period_ns( min_sampling_period_ns );
		add_blocks( *desc );
	}

	uint32_t counter_id = 0;

	/// @brief Adds an int value to the current counter
	auto add_int = [desc, event, &counter_id] (
		const counter_t value,
		const std::string& name,
		GpuCounterDescriptor::MeasureUnit unit = GpuCounterDescriptor::NONE )
	{
		// Send description of the counter in first packet
		if ( desc )
		{
			auto spec = desc->add_specs();
			spec->set_counter_id( counter_id );
			std::string num = "00. ";
			std::sprintf( num.data(), "%02u", counter_id );
			num[2] = '.';
			spec->set_name( num + name );
			spec->add_numerator_units( unit );
		}
		else
		{
			auto counter = event->add_counters();
			counter->set_counter_id( counter_id );
			counter->set_int_value( value );
		}

		++counter_id;
	};

	/// @brief Adds a double value to the current counter
	auto add_double = [desc, event, &counter_id] (
		double value,
		const std::string& name,
		GpuCounterDescriptor::MeasureUnit unit = GpuCounterDescriptor::NONE )
	{
		// Send descriptor in first packet
		if ( desc )
		{
			auto spec = desc->add_specs();
			spec->set_counter_id( counter_id );
			std::string num = "00. ";
			std::sprintf( num.data(), "%02u", counter_id );
			num[2] = '.';
			spec->set_name( num + name );
			spec->add_numerator_units( unit );
		}
		else
		{
			auto counter = event->add_counters();
			counter->set_counter_id( counter_id );

			if ( unit == GpuCounterDescriptor::PERCENT )
			{
				value *= 100.0;
			}
			counter->set_double_value( value );
		}

		++counter_id;
	};

	auto gpu_active = data_source->get_counter( Counter::GPU_ACTIVE );
	add_int( gpu_active, "GPU active" );

	auto add_percentage = [&add_double]( counter_t numerator, counter_t denominator, const char* name )
	{
		double percentage = 0.0;
		if ( denominator >= 1.0 )
		{
			percentage = numerator / double( denominator );
		}
		add_double( percentage, name, GpuCounterDescriptor::PERCENT );
	};

	add_int( data_source->get_counter( Counter::JS0_ACTIVE ), "JS0 active" );
	add_int( data_source->get_counter( Counter::JS1_ACTIVE ), "JS1 active" );
	add_int( data_source->get_counter( Counter::JS2_ACTIVE ), "JS2 active" );

	add_int( data_source->get_counter( Counter::IRQ_ACTIVE ), "IRQ active" );

	auto js0_tasks = data_source->get_counter( Counter::JS0_TASKS );
	add_int( js0_tasks, "JS0 tasks");
	// Pixel count
	/// @todo Tile size
	/// 32x32 on Mali-T760, Mali-T800
	/// 16x16 on Mali-T600, Mali-T620, Mali-T720
	uint32_t tile_size = 32 * 32;
	add_int( data_source->get_counter( Counter::JS0_TASKS ) * 32 * 32, "Pixel count" );

	add_int( data_source->get_counter( Counter::JS1_TASKS ), "JS1 tasks" );
	add_int( data_source->get_counter( Counter::JS2_TASKS ), "JS2 tasks" );

	add_int( data_source->get_counter( Counter::FRAG_ACTIVE ), "Fragment active" );
	add_int( data_source->get_counter( Counter::COMPUTE_ACTIVE ), "Compute active" );

	auto tripipe_active = data_source->get_counter( Counter::TRIPIPE_ACTIVE );
	add_int( tripipe_active, "Tripipe active" );
	add_percentage( tripipe_active, gpu_active, "Tripipe usage" );

	// Compute tasks
	add_int( data_source->get_counter( Counter::COMPUTE_TASKS ), "Compute tasks" );
	// Compute threads
	add_int( data_source->get_counter( Counter::COMPUTE_THREADS ), "Compute threads" );

	// Primitives read from the tile list
	add_int( data_source->get_counter( Counter::FRAG_PRIMITIVES ), "Frag primitives" );
	// Primitives not relevant for the current tile
	add_int( data_source->get_counter( Counter::FRAG_PRIMITIVES_DROPPED ), "Frag primitives dropped" );
	// 2x2 pixel quad rasterized
	add_int( data_source->get_counter( Counter::FRAG_QUADS_RAST ), "Frag quad rasterized" );
	// 2x2 pixel quad early depth-stencil tested
	add_int( data_source->get_counter( Counter::FRAG_QUADS_EZS_TEST ), "Frag quads EZS test" );
	// 2x2 pixel quad killed by early depth-stencil test
	add_int( data_source->get_counter( Counter::FRAG_QUADS_EZS_KILLED ), "Frag quads EZS killed" );
	// ZS unit blocked for no physical tile memory available
	add_int( data_source->get_counter( Counter::FRAG_CYCLES_NO_TILE ), "Frag cycles no tile" );
	// Fragment thread created by the GPU
	add_int( data_source->get_counter( Counter::FRAG_THREADS ), "Frag threads" );
	// Threads with no sample coverage
	add_int( data_source->get_counter( Counter::FRAG_DUMMY_THREADS ), "Frag dummy threads" );

	// Threads triggering late ZS test
	add_int( data_source->get_counter( Counter::FRAG_THREADS_LZS_TEST ), "Frag threads LZS test" );
	// Threads killed by ZS test
	add_int( data_source->get_counter( Counter::FRAG_THREADS_LZS_KILLED ), "Frag threads LZS killed" );
	// 16x16 (or 32x32) pixel tiles rendered by shader core
	add_int( data_source->get_counter( Counter::FRAG_NUM_TILES ), "Frag num tiles" );
	// Pixel tile writeback cancelled due to maching hash
	add_int( data_source->get_counter( Counter::FRAG_TRANS_ELIM ), "Frag transaction eliminations" );

	// Arithmetic hardware
	auto arith_words = data_source->get_counter( Counter::ARITH_WORDS );
	add_int( arith_words, "Arithmetic words" );
	add_percentage( arith_words, tripipe_active, "Arithmetic usage" );

	// Architectural utilization of load/store pipe
	auto ls_words = data_source->get_counter( Counter::LS_WORDS );
	add_int( ls_words, "LS words" );
	add_percentage( ls_words, tripipe_active, "LS architectural usage" );

	// Microarchitectural utilization of load/store pipe
	auto ls_issues = data_source->get_counter( Counter::LS_ISSUES );
	add_int( ls_issues, "LS issues" );
	add_percentage( ls_issues, tripipe_active, "LS microarchitectural usage" );

	// Load/store cycles per instruction
	double ls_cpi = 0.0;
	if ( ls_words )
	{
		ls_cpi = ls_issues / double( ls_words );
	}
	add_double( ls_cpi, "LS pipe CPI" );

	// Load/Store cache events
	// Hitrate: percentage of hits vs total number of accesses

	// Read hitrate
	auto read_ops = data_source->get_counter( Counter::LSC_READ_OP );
	add_int( read_ops, "LSC reads" );
	add_percentage(
		data_source->get_counter( Counter::LSC_READ_HITS ),
		read_ops,
		"LSC read hitrate" );

	// Write hitrate
	auto write_ops = data_source->get_counter( Counter::LSC_WRITE_OP );
	add_int( write_ops, "LSC writes" );
	add_percentage(
		data_source->get_counter( Counter::LSC_WRITE_HITS ),
		write_ops,
		"LSC write hitrate" );

	// Atomic hitrate
	auto atomic_ops = data_source->get_counter( Counter::LSC_ATOMIC_OP );
	add_int( atomic_ops, "LSC atomics" );
	add_percentage(
		data_source->get_counter( Counter::LSC_ATOMIC_HITS ),
		atomic_ops,
		"LSC atomic hitrate" );

	// Lines fetched by L1 from L2
	add_int( data_source->get_counter( Counter::LSC_LINE_FETCHES ), "LSC lines fetched" );
	// Dirty lines evicted from L1 into L2
	add_int( data_source->get_counter( Counter::LSC_DIRTY_LINE ), "LSC dirty lines" );
	// Snoops into L1 from L2
	add_int( data_source->get_counter( Counter::LSC_SNOOPS ), "LSC snoops" );
	
	// Texture instruction executed
	auto tex_words = data_source->get_counter( Counter::TEX_WORDS );
	add_int( tex_words, "Texture instructions" );

	// Texture issue cycles used
	auto tex_issues = data_source->get_counter( Counter::TEX_ISSUES );
	add_int( tex_issues, "Texture issue cycles" );

	// Texture pipe CPI
	double tex_cpi = 0.0;
	if ( tex_words )
	{
		tex_cpi = tex_issues / double( tex_words );
	}
	add_double( tex_cpi, "Texture pipe CPI" );

	// Primitives
	add_int( data_source->get_counter( Counter::TI_POINTS ), "Point primitives" );
	add_int( data_source->get_counter( Counter::TI_LINES ), "Line primitives" );
	add_int( data_source->get_counter( Counter::TI_TRIANGLES ), "Triangle primitives" );

	// Visibility and culling
	add_int( data_source->get_counter( Counter::TI_PRIM_VISIBLE ), "Primitives visible" );
	add_int( data_source->get_counter( Counter::TI_PRIM_CULLED ), "Primitives culled" );
	add_int( data_source->get_counter( Counter::TI_PRIM_CLIPPED ), "Primitives clipped" );
	add_int( data_source->get_counter( Counter::TI_FRONT_FACING ), "Front-facing triangles" );
	add_int( data_source->get_counter( Counter::TI_BACK_FACING ), "Back-facing triangles" );

	// L2 cache
	auto read_lookup = data_source->get_counter( Counter::L2_READ_LOOKUP );
	add_int( read_lookup, "L2 read lookups" );
	add_percentage(
		data_source->get_counter( Counter::L2_READ_HIT ),
		read_lookup,
		"L2 read hitrate" );
	add_int( data_source->get_counter( Counter::L2_READ_SNOOP ), "L2 read snoops" );

	auto write_lookup = data_source->get_counter( Counter::L2_WRITE_LOOKUP );
	add_int( write_lookup, "L2 write lookups" );
	add_percentage(
		data_source->get_counter( Counter::L2_WRITE_HIT ),
		write_lookup,
		"L2 write hitrate" );
	add_int( data_source->get_counter( Counter::L2_WRITE_SNOOP ), "L2 write snoops" );

	/// @todo Some implementations use 8 bytes interfaces instead of 16
	uint32_t l2_axi_width_bytes = 16;
	/// @todo RK3399 has a Mali T860 MP4 (quad-core)
	uint32_t l2_axi_port_count = 4;

	auto read_beats = data_source->get_counter( Counter::L2_EXT_READ_BEATS );
	add_int(
		read_beats * l2_axi_width_bytes,
		"L2 external reads",
		GpuCounterDescriptor::BYTE );
	add_percentage(
		read_beats,
		l2_axi_port_count * gpu_active,
		"L2 external read usage" );
	add_int( data_source->get_counter( Counter::L2_EXT_AR_STALL ), "L2 external read stalls" );

	auto write_beats = data_source->get_counter( Counter::L2_EXT_WRITE_BEATS );
	add_int(
		write_beats * l2_axi_width_bytes,
		"L2 external writes",
		GpuCounterDescriptor::BYTE );
	add_percentage(
		write_beats,
		l2_axi_port_count * gpu_active,
		"L2 external write usage" );
	add_int( data_source->get_counter( Counter::L2_EXT_W_STALL ), "L2 external write stalls" );

	// At the end mark first packet sent
	state->was_cleared = false;
}

} // namespace pps::gpu

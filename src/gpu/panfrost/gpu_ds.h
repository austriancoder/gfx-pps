/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <pps/pds.h>

#include "counters.h"

namespace pps::gpu
{
struct PanfrostIncrementalState
{
	bool was_cleared = true;
};

struct PanfrostDataSourceTraits : public perfetto::DefaultDataSourceTraits
{
	using IncrementalStateType = PanfrostIncrementalState;
};

using counter_t = uint32_t;

class PanfrostDataSource
: public perfetto::DataSource<PanfrostDataSource, PanfrostDataSourceTraits>
{
  public:
	static constexpr const char* get_name() { return "gpu.metrics"; }

	static uint64_t sampling_period_ns;

	void OnSetup( const SetupArgs& args ) override;
	void OnStart( const StartArgs& args ) override;
	void OnStop( const StopArgs& args ) override;

	static State get_state();

	/// @return The value of the counter c
	counter_t get_counter( Counter c );

	/// @brief Perfetto trace callback
	static void trace_callback( TraceContext ctx );

  private:
	/// Minimum sampling period is 1 millisecond
	static constexpr uint64_t min_sampling_period_ns = 1000000;

	/// @brief Samples performance counters which means
	/// asking the GPU to dump them to a user space buffer
	void dump();

	counter_t* counters = nullptr;
	std::vector<const char*> counter_names = std::vector<const char*>();

	int card_fd = -1;

	std::allocator<counter_t> allocator;
};

}  // namespace pps::gpu

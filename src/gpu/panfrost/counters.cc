#include "counters.h"

#include <cassert>


namespace pps::gpu
{


const char* to_string( const CounterBlock block )
{
	switch ( block )
	{
	case CounterBlock::UNDEFINED: return "UNDEFINED";
	case CounterBlock::JOB_MANAGER: return "JOB_MANAGER";
	case CounterBlock::SHADER_CORE: return "SHADER_CORE";
	case CounterBlock::FRAG_FRONTEND: return "FRAG_FRONTEND";
	case CounterBlock::FRAG_BACKEND: return "FRAG_BACKEND";
	case CounterBlock::ARITHMETIC: return "ARITHMETIC";
	case CounterBlock::LOAD_STORE: return "LOAD_STORE";
	case CounterBlock::LOAD_STORE_CACHE: return "LOAD_STORE_CACHE";
	case CounterBlock::TEXTURE: return "TEXTURE";
	case CounterBlock::TILER_PRIMITIVE: return "TILER_PRIMITIVE";
	case CounterBlock::TILER_VISIBILITY: return "TILER_VISIBILITY";
	case CounterBlock::L2_CACHE_INTERNAL: return "L2_CACHE_INTERNAL";
	case CounterBlock::L2_CACHE_EXTERNAL: return "L2_CACHE_EXTERNAL";
	default:
		assert( false && "Invalid counter" );
		return "INVALID_BLOCK";
	}
}


/// @return A string representation for the counter
const char* to_string( const Counter counter )
{
	switch ( counter )
	{
	case Counter::GPU_ACTIVE: return "GPU_ACTIVE";
	case Counter::JS0_ACTIVE: return "JS0_ACTIVE";
	case Counter::JS1_ACTIVE: return "JS1_ACTIVE";
	case Counter::JS2_ACTIVE: return "JS2_ACTIVE";
	case Counter::IRQ_ACTIVE: return "IRQ_ACTIVE";
	case Counter::JS0_TASKS: return "JS0_TASKS";
	case Counter::JS1_TASKS: return "JS1_TASKS";
	case Counter::JS2_TASKS: return "JS2_TASKS";
	case Counter::FRAG_ACTIVE: return "FRAG_ACTIVE";
	case Counter::COMPUTE_ACTIVE: return "COMPUTE_ACTIVE";
	case Counter::TRIPIPE_ACTIVE: return "TRIPIPE_ACTIVE";
	case Counter::COMPUTE_TASKS: return "COMPUTE_TASKS";
	case Counter::COMPUTE_THREADS: return "COMPUTE_THREADS";
	case Counter::FRAG_PRIMITIVES: return "FRAG_PRIMITIVES";
	case Counter::FRAG_PRIMITIVES_DROPPED: return "FRAG_PRIMITIVES_DROPPED";
	case Counter::FRAG_QUADS_RAST: return "FRAG_QUADS_RAST";
	case Counter::FRAG_QUADS_EZS_TEST: return "FRAG_QUADS_EZS_TEST";
	case Counter::FRAG_QUADS_EZS_KILLED: return "FRAG_QUADS_EZS_KILLED";
	case Counter::FRAG_CYCLES_NO_TILE: return "FRAG_CYCLES_NO_TILE";
	case Counter::FRAG_THREADS: return "FRAG_THREADS";
	case Counter::FRAG_DUMMY_THREADS: return "FRAG_DUMMY_THREADS";
	case Counter::FRAG_THREADS_LZS_TEST: return "FRAG_THREADS_LZS_TEST";
	case Counter::FRAG_THREADS_LZS_KILLED: return "FRAG_THREADS_LZS_KILLED";
	case Counter::FRAG_NUM_TILES: return "FRAG_NUM_TILES";
	case Counter::FRAG_TRANS_ELIM: return "FRAG_TRANS_ELIM";
	case Counter::ARITH_WORDS: return "ARITH_WORDS";
	case Counter::LS_WORDS: return "LS_WORDS";
	case Counter::LS_ISSUES: return "LS_ISSUES";
	case Counter::LSC_READ_HITS: return "LSC_READ_HITS";
	case Counter::LSC_READ_OP: return "LSC_READ_OP";
	case Counter::LSC_WRITE_HITS: return "LSC_WRITE_HITS";
	case Counter::LSC_WRITE_OP: return "LSC_WRITE_OP";
	case Counter::LSC_ATOMIC_HITS: return "LSC_ATOMIC_HITS";
	case Counter::LSC_ATOMIC_OP: return "LSC_ATOMIC_OP";
	case Counter::LSC_LINE_FETCHES: return "LSC_LINE_FETCHES";
	case Counter::LSC_DIRTY_LINE: return "LSC_DIRTY_LINE";
	case Counter::LSC_SNOOPS: return "LSC_SNOOPS";
	case Counter::TEX_WORDS: return "TEX_WORDS";
	case Counter::TEX_ISSUES: return "TEX_ISSUES";
	case Counter::TI_POINTS: return "TI_POINTS";
	case Counter::TI_LINES: return "TI_LINES";
	case Counter::TI_TRIANGLES: return "TI_TRIANGLES";
	case Counter::TI_PRIM_VISIBLE: return "TI_PRIM_VISIBLE";
	case Counter::TI_PRIM_CULLED: return "TI_PRIM_CULLED";
	case Counter::TI_PRIM_CLIPPED: return "TI_PRIM_CLIPPED";
	case Counter::TI_FRONT_FACING: return "TI_FRONT_FACING";
	case Counter::TI_BACK_FACING: return "TI_BACK_FACING";
	case Counter::L2_READ_LOOKUP: return "L2_READ_LOOKUP";
	case Counter::L2_READ_HIT: return "L2_READ_HIT";
	case Counter::L2_READ_SNOOP: return "L2_READ_SNOOP";
	case Counter::L2_WRITE_LOOKUP: return "L2_WRITE_LOOKUP";
	case Counter::L2_WRITE_HIT: return "L2_WRITE_HIT";
	case Counter::L2_WRITE_SNOOP: return "L2_WRITE_SNOOP";
	case Counter::L2_EXT_READ_BEATS: return "L2_EXT_READ_BEATS";
	case Counter::L2_EXT_AR_STALL: return "L2_EXT_AR_STALL";
	case Counter::L2_EXT_WRITE_BEATS: return "L2_EXT_WRITE_BEATS";
	case Counter::L2_EXT_W_STALL: return "L2_EXT_W_STALL";
	default:
		assert( false && "Invalid counter" );
		return "INVALID_COUNTER";
	}
}


CounterBlock get_block( Counter counter )
{
	switch ( counter )
	{
	// Job manager
	case Counter::GPU_ACTIVE:
	case Counter::JS0_ACTIVE:
	case Counter::JS1_ACTIVE:
	case Counter::JS2_ACTIVE:
	case Counter::IRQ_ACTIVE:
	case Counter::JS0_TASKS:
	case Counter::JS1_TASKS:
	case Counter::JS2_TASKS:
		return CounterBlock::JOB_MANAGER;

	// Shader core
	case Counter::FRAG_ACTIVE:
	case Counter::COMPUTE_ACTIVE:
	case Counter::TRIPIPE_ACTIVE:
	case Counter::COMPUTE_TASKS:
	case Counter::COMPUTE_THREADS:
		return CounterBlock::SHADER_CORE;

	// Frag frontend
	case Counter::FRAG_PRIMITIVES:
	case Counter::FRAG_PRIMITIVES_DROPPED:
	case Counter::FRAG_QUADS_RAST:
	case Counter::FRAG_QUADS_EZS_TEST:
	case Counter::FRAG_QUADS_EZS_KILLED:
	case Counter::FRAG_CYCLES_NO_TILE:
	case Counter::FRAG_THREADS:
	case Counter::FRAG_DUMMY_THREADS:
		return CounterBlock::FRAG_FRONTEND;

	// Frag backend
	case Counter::FRAG_THREADS_LZS_TEST:
	case Counter::FRAG_THREADS_LZS_KILLED:
	case Counter::FRAG_NUM_TILES:
	case Counter::FRAG_TRANS_ELIM:
		return CounterBlock::FRAG_BACKEND;

	// Arithmetic pipe events
	case Counter::ARITH_WORDS:
		return CounterBlock::ARITHMETIC;

	// Load/Store pipe events
	case Counter::LS_WORDS:
	case Counter::LS_ISSUES:
		return CounterBlock::LOAD_STORE;

	// Load/Store cache events
	case Counter::LSC_READ_HITS:
	case Counter::LSC_READ_OP:
	case Counter::LSC_WRITE_HITS:
	case Counter::LSC_WRITE_OP:
	case Counter::LSC_ATOMIC_HITS:
	case Counter::LSC_ATOMIC_OP:
	case Counter::LSC_LINE_FETCHES:
	case Counter::LSC_DIRTY_LINE:
	case Counter::LSC_SNOOPS:
		return CounterBlock::LOAD_STORE_CACHE;

	// Texture pipe events
	case Counter::TEX_WORDS:
	case Counter::TEX_ISSUES:
		return CounterBlock::TEXTURE;

	// Tiler primitive occurrence
	case Counter::TI_POINTS:
	case Counter::TI_LINES:
	case Counter::TI_TRIANGLES:
		return CounterBlock::TILER_PRIMITIVE;

	// Tiler visibility and culling occurence
	case Counter::TI_PRIM_VISIBLE:
	case Counter::TI_PRIM_CULLED:
	case Counter::TI_PRIM_CLIPPED:
	case Counter::TI_FRONT_FACING:
	case Counter::TI_BACK_FACING:
		return CounterBlock::TILER_VISIBILITY;

	// L2 cache internal traffic events
	case Counter::L2_READ_LOOKUP:
	case Counter::L2_READ_HIT:
	case Counter::L2_READ_SNOOP:
	case Counter::L2_WRITE_LOOKUP:
	case Counter::L2_WRITE_HIT:
	case Counter::L2_WRITE_SNOOP:
		return CounterBlock::L2_CACHE_INTERNAL;

	// L2 cache external traffic events
	case Counter::L2_EXT_READ_BEATS:
	case Counter::L2_EXT_AR_STALL:
	case Counter::L2_EXT_WRITE_BEATS:
	case Counter::L2_EXT_W_STALL:
		return CounterBlock::L2_CACHE_EXTERNAL;

	default:
		assert( false && "Invalid counter" );
		return CounterBlock::UNDEFINED;
	}
}


} // namespace pps::gpu

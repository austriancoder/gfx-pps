/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <cstdint>


namespace pps::gpu
{


enum class CounterBlock
{
	UNDEFINED,

	JOB_MANAGER,
	SHADER_CORE,
	FRAG_FRONTEND,
	FRAG_BACKEND,
	ARITHMETIC,
	LOAD_STORE,
	LOAD_STORE_CACHE,
	TEXTURE,
	TILER_PRIMITIVE,
	TILER_VISIBILITY,
	L2_CACHE_INTERNAL,
	L2_CACHE_EXTERNAL,

	MAX_VALUE
};


/// @return A string representation for the counter block
const char* to_string( CounterBlock block );


enum class Counter
{
	// Job manager
	GPU_ACTIVE,
	JS0_ACTIVE,
	JS1_ACTIVE,
	JS2_ACTIVE,
	IRQ_ACTIVE,
	JS0_TASKS,
	JS1_TASKS,
	JS2_TASKS,

	// Shader core
	FRAG_ACTIVE,
	COMPUTE_ACTIVE,
	TRIPIPE_ACTIVE,
	COMPUTE_TASKS,
	COMPUTE_THREADS,

	// Frag frontend
	FRAG_PRIMITIVES,
	FRAG_PRIMITIVES_DROPPED,
	FRAG_QUADS_RAST,
	FRAG_QUADS_EZS_TEST,
	FRAG_QUADS_EZS_KILLED,
	FRAG_CYCLES_NO_TILE,
	FRAG_THREADS,
	FRAG_DUMMY_THREADS,

	// Frag backend
	FRAG_THREADS_LZS_TEST,
	FRAG_THREADS_LZS_KILLED,
	FRAG_NUM_TILES,
	FRAG_TRANS_ELIM,

	// Arithmetic pipe events
	ARITH_WORDS,

	// Load/Store pipe events
	LS_WORDS,
	LS_ISSUES,

	// Load/Store cache events
	LSC_READ_HITS,
	LSC_READ_OP,
	LSC_WRITE_HITS,
	LSC_WRITE_OP,
	LSC_ATOMIC_HITS,
	LSC_ATOMIC_OP,
	LSC_LINE_FETCHES,
	LSC_DIRTY_LINE,
	LSC_SNOOPS,

	// Texture pipe events
	TEX_WORDS,
	TEX_ISSUES,

	// Tiler primitive occurrence
	TI_POINTS,
	TI_LINES,
	TI_TRIANGLES,

	// Tiler visibility and culling occurence
	TI_PRIM_VISIBLE,
	TI_PRIM_CULLED,
	TI_PRIM_CLIPPED,
	TI_FRONT_FACING,
	TI_BACK_FACING,

	// L2 cache internal traffic events
	L2_READ_LOOKUP,
	L2_READ_HIT,
	L2_READ_SNOOP,
	L2_WRITE_LOOKUP,
	L2_WRITE_HIT,
	L2_WRITE_SNOOP,

	// L2 cache external traffic events
	L2_EXT_READ_BEATS,
	L2_EXT_AR_STALL,
	L2_EXT_WRITE_BEATS,
	L2_EXT_W_STALL,

	// Keep it last
	MAX_VALUE,
};


/// @return A string representation for the counter
const char* to_string( Counter counter );


/// @return The block this counter belongs to
CounterBlock get_block( Counter counter );

} // namespace pps::gpu

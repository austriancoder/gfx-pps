# Copyright (c) 2020 Collabora, Ltd.
# Author: Antonio Caggiano <antonio.caggiano@collabora.com>
#
# SPDX-License-Identifier: GPL-2.0-only

set( TEST_DATA_PATHS
	timeline-presentation-f.log
	timeline-presentation-i.log
	timeline-presentation-p.log )

set( WESGR_URL "https://raw.githubusercontent.com/ppaalanen/wesgr/master/testdata/" )
# Download function
foreach( DATA_PATH ${TEST_DATA_PATHS} )
	set( DATA_OUT ${TEST_DATA_DIR}/${DATA_PATH} )
	if( NOT EXISTS ${DATA_OUT} )
		message( STATUS "Downloading ${DATA_PATH}" )
		set( DATA_URL "${WESGR_URL}/${DATA_PATH}" )
		file( DOWNLOAD ${DATA_URL} ${DATA_OUT} )
	endif()
endforeach()

/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <gtest/gtest.h>
#include <weston/timeline_ds.h>

namespace pps::weston
{


TEST( Timeline, Vblank )
{
	auto read_path = "test/data/vblank.log";
	auto read_file = std::fopen( read_path, "r" );
	EXPECT_TRUE( read_file != nullptr );

	TimelineDataSource data_source;
	auto timepoints = data_source.read_timepoints( fileno( read_file ) );
	EXPECT_EQ( timepoints.size(), 3 );

	auto& parser = data_source.get_parser();
	EXPECT_EQ( parser.get_objects().size(), 1 );

	auto repaint_end = timepoints[0];
	EXPECT_EQ( repaint_end.type, WestonTimepoint::Type::CORE_KMS_FLIP_END );

	auto vblank_begin = timepoints[1];
	EXPECT_EQ( vblank_begin.type, WestonTimepoint::Type::VBLANK_BEGIN );

	auto vblank_end = timepoints[2];
	EXPECT_EQ( vblank_end.type, WestonTimepoint::Type::VBLANK_END );

	EXPECT_EQ( vblank_begin.get_timestamp() - repaint_end.get_timestamp(), 50500 );
}


class TimelineFixture : public ::testing::TestWithParam<const char*>
{
  protected:
	TimelineDataSource data_source;
};


TEST_P( TimelineFixture, ReadFile )
{
	auto read_path = GetParam();
	auto read_file = std::fopen( read_path, "r" );
	EXPECT_TRUE( read_file != nullptr );

	auto timepoints = data_source.read_timepoints( fileno( read_file ) );
	EXPECT_TRUE( timepoints.size() > 0 );

	auto& parser = data_source.get_parser();
	EXPECT_TRUE( parser.get_objects().size() > 0 );
}


INSTANTIATE_TEST_SUITE_P(
	TimelineReadFile,
	TimelineFixture,
	::testing::Values(
		"test/data/wesgr/timeline-3.log",
		"test/data/wesgr/timeline-presentation-f.log",
		"test/data/wesgr/timeline-presentation-i.log",
		"test/data/wesgr/timeline-presentation-p.log",
		"test/data/timeline-presentation-f-throttle.log",
		"test/data/timeline-presentation-p-idle.log",
		"test/data/timeline-presentation-p-throttle.log"
	) );


} // namespace pps::weston


int main()
{
	testing::InitGoogleTest();
	return RUN_ALL_TESTS();
}
